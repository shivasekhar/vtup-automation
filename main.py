from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import argparse
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time
import names

def get_element_by_xpath_with_wait(driver, xpath: str, wait_secs: int):
    try:
        element = WebDriverWait(driver, wait_secs).until(
            EC.presence_of_element_located((By.XPATH, xpath))
        )
        return element
    except Exception as e:
        print(e)
    return

def main(driver, url, name, chat_period, product_add_period, like_period, use_video: bool, use_audio: bool):
    driver.get(url)

    for entry in driver.get_log('browser'):
        print(entry)
    # greeting page
    attendeeNameElement = driver.find_element_by_id("viewerName")
    attendeeEmailElement = driver.find_element_by_id("viewerEmail")
    attendeeAcceptElement = driver.find_element_by_id('userAccept')
    attendeeSubmitElements = driver.find_elements_by_xpath('//button[@type="submit"]')

    if len(attendeeSubmitElements) < 1:
        print('Submit button not found')


    attendeeNameElement.send_keys(name)
    attendeeEmailElement.send_keys(f'{name}_automation@coda.global')
    attendeeAcceptElement.click()
    attendeeSubmitElements[0].click()

    print('submitted details')
    # meeting rediness
    try:
        proceedButtonElement = get_element_by_xpath_with_wait(driver, '//button[contains(text(), "Proceed")]', 30)
        proceedButtonElement.click()
    except:
        print('proceed button action not perfomed')

    # meeting page
    # ugly hack to handle difference in active element based on resolution
    print("in meeting page")
    time.sleep(10)
    if use_video:
        print("enabling video")
        video_action(driver, enable=True)
    
    commands = {
        'VIDEO_ON': lambda: video_action(driver, True),
        'VIDEO_OFF': lambda: video_action(driver, False),
        'AUDIO_ON': lambda: audio_action(driver, True),
        'AUDIO_OFF': lambda: audio_action(driver, False)
    }
    counter = 0
    last_command = None
    print('starting loop')
    while True:
        counter += 1
        time.sleep(0.5)
        if counter % 10 == 0:
            print(f'counter = {counter}')
        if counter % chat_period == 0:
            send_message(driver, f'{name}:{counter}: msg activity')
        
        if counter % chat_period == 0:
            add_product(driver)

        if counter % like_period == 0:
            click_like(driver)
            

        command_elements = driver.find_elements_by_xpath(f'//div[@class="chat-history py-3"]//p[contains(text(), "COMMAND::{name}::")]')
        if len(command_elements) != 0:
            command = command_elements[-1].get_attribute('innerHTML').replace(f'COMMAND::{name}::', '', 1)
            if command != last_command:
                print(f'command : {command}')
                action = commands.get(command)
                last_command = command
                if action is None:
                    send_message(driver, f'{name}:{command}: not available')
                else:
                    try:
                        action()
                    except:
                        send_message(driver, f'{name}:{command}: error performing action')

    time.sleep(60)



def send_message(driver, msg=None):
    chatInputElement = get_element_by_xpath_with_wait(driver, '//input[@type="text"]', 10)
    chatSendElement = get_element_by_xpath_with_wait(driver, '//div[@class="send-img"]', 10)
    chatInputElement.send_keys(msg)
    chatSendElement.click()

def add_product(driver):
    productAddElement = get_element_by_xpath_with_wait(driver, '(//img[contains(@src, "icon_add")])[1]', 10)
    productAddElement.click()

def click_like(driver):
    likeElement = get_element_by_xpath_with_wait(driver, "//img[contains(@class, 'like-icon')]", 10)
    likeElement.click()


def video_action(driver, enable=True):
    text = "Start Video"
    if not enable:
        text = "Stop Video"
    try:
        startVideoButtonElement = get_element_by_xpath_with_wait(driver, f'//span[contains(text(), "{text}")]', 10)
        startVideoButtonElement.click()
    except:
        startVideoButtonElement = get_element_by_xpath_with_wait(driver, f'//p[contains(text(), "{text}")]', 10)
        startVideoButtonElement.click()

def audio_action(driver, enable=True):
    text = "Unmute"
    if not enable:
        text = "Mute"
    try:
        startVideoButtonElement = get_element_by_xpath_with_wait(driver, f'//span[contains(text(), "{text}")]', 10)
        startVideoButtonElement.click()
    except:
        startVideoButtonElement = get_element_by_xpath_with_wait(driver, f'//p[contains(text(), "{text}")]', 10)
        startVideoButtonElement.click()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Automation script')
    parser.add_argument('--url', help='meeting url')
    parser.add_argument('--msg-period', help='chat message period in seconds', default=50, type=int)
    parser.add_argument('--product-period', help='product card add period in seconds', default=50, type=int)
    parser.add_argument('--like-period', help='send like activity period', default=30, type=int)
    parser.add_argument('--video', help='join with video', action="store_true")
    parser.add_argument('--audio', help='join with audio', action="store_true")
    args = parser.parse_args()


    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36'
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--use-fake-ui-for-media-stream")
    chrome_options.add_argument("--use-fake-device-for-media-stream")
    chrome_options.add_argument("--allow-file-access")
    chrome_options.add_argument("--agc-startup-min-volume=22")
    chrome_options.add_argument("--disable-volume-adjust-sound")
    chrome_options.add_argument('headless')
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument('window-size=1920,1080')
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument(f'user-agent={user_agent}')
    chrome_options.add_argument('--disable-web-security')
    chrome_options.add_argument("--allow-file-access-from-files")
    chrome_options.add_argument("--use-file-for-fake-video-capture=./assets/video.y4m")
    chrome_options.add_argument("--use-file-for-fake-audio-capture=./assets/audio.wav")


    print(args)

    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(10) 
    
    name = names.get_first_name()
    try:
        main(driver, args.url, name, args.msg_period, args.product_period, args.like_period, args.video, args.audio)
    finally:
        driver.close()
