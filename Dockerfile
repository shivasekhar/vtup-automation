FROM joyzoursky/python-chromedriver:3.9

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY assets assets
COPY main.py main.py

CMD ["python"]
